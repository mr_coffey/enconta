defmodule EncontaTest do
  use ExUnit.Case
  doctest Enconta

  setup do
    {:ok, %{id: "4e25ce61-e6e2-457a-89f7-116404990967", start: "2017-01-01", finish: "2017-01-11"}}
  end

  test "greets the world", %{id: id, start: start, finish: finish} do
    assert Enconta.fetch_orders(id, start, finish) >= 44
  end
end
