defmodule Enconta do
  @doc """
  Hello world.

  ## Examples

      iex> Enconta.fetch_orders("4e25ce61-e6e2-457a-89f7-116404990967", "2017-01-01", "2017-01-11")
      "44"

  """
  def fetch_orders(id, start, finish) do
    # TODO: Base url should be a envar
    mailer_host = "http://34.209.24.195"
    response = HTTPoison.get("#{mailer_host}/facturas?id=#{id}&start=#{start}&finish=#{finish}")
    acc_bills(response, acc)
  end

  def acc_bills({:ok, %HTTPoison.Response{status_code: status_code} = response}, acc \\ []) when status_code == 200 do
    Enum.into(acc, response.body)
  end

  def acc_bills({:error, _}) do
    :error
  end
end
